
bl_info = {
        "name": "Auto Texture Size",
        "description": "Downscale textures depending on the distance.",
        "author": "Christophe Seux",
        "version": (1, 0),
        "blender": (2, 91, 0),
        "location": "Properties > Render > Auto Texture Size",
        "warning": "", # used for warning icon and text in add-ons panel
        "wiki_url": "http://my.wiki.url",
        "tracker_url": "http://my.bugtracker.url",
        "support": "COMMUNITY",
        "category": "Render"
        }

import bpy
from bpy.props import PointerProperty
from bpy.app.handlers import persistent

from .ui import ATS_PT_panel
from .operators import ATS_OT_set_textures_size, ATS_OT_restore_textures,\
ATS_OT_set_materials_color, ATS_OT_restore_materials
from .properties import ATS_PG_properties
from os.path import abspath, exists

classes = [
    ATS_PG_properties,
    ATS_PT_panel,
    ATS_OT_set_textures_size,
    ATS_OT_restore_textures,
    ATS_OT_set_materials_color,
    ATS_OT_restore_materials
]


@persistent
def render_pre(dummy):
    print("Render Pre Handler, check if all textures have the good size:")

    data = bpy.context.scene.auto_texture_size.data

    for img_src_path, img_data in data.items() :
        #print('\n', img_src_path, '\n', img_data)
        dst_path = img_data.get('dst_path')
        if not dst_path :
            continue

        dst_abs_path = abspath( bpy.path.abspath(dst_path) )

        images = [i for i in bpy.data.images if i.filepath and bpy.path.relpath(i.filepath)==img_src_path]

        for img in images :
            if dst_path and img_src_path != dst_path and exists(dst_abs_path):
                img.filepath = dst_path


def register():
    for cls in classes :
        bpy.utils.register_class(cls)
    bpy.types.Scene.auto_texture_size = PointerProperty(type=ATS_PG_properties)
    bpy.app.handlers.render_pre.append(render_pre)

def unregister():
    bpy.app.handlers.render_pre.remove(render_pre)
    del bpy.types.Scene.auto_texture_size
    for cls in reversed(classes) :
        bpy.utils.unregister_class(cls)

    

if __name__ == '__main__':
    register()
