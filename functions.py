import bpy
from math import tan, pow, ceil, log, log2, floor, atan, ceil
from mathutils import Vector
from pathlib import Path
from os.path import abspath, basename, exists, dirname
import re


def restore_images():
    for i in bpy.data.images :
        if not i.filepath :
            continue 

        if 'src_path' in i.keys() :
            src_path = abspath(bpy.path.abspath(i['src_path']) )
            path = abspath(bpy.path.abspath(i.filepath, library=i.library))

            if exists(src_path) :
                i.filepath = i['src_path']
            elif exists(path) :
                i['src_path'] = bpy.path.relpath(path)

            
        
# Recursive function
def store_optimal_textures(data):
    scene = bpy.context.scene

    cam = scene.camera
    cam_origin = cam.matrix_world.to_translation()

    depsgraph = bpy.context.evaluated_depsgraph_get()

    res_width = scene.render.resolution_percentage * scene.render.resolution_x * 0.01
    print('res_width',res_width)

    for ob_inst in depsgraph.object_instances:
        if ob_inst.is_instance:  # Real dupli instance
            ob = ob_inst.instance_object.original
            mat = ob_inst.matrix_world.copy()
        else:  # Usual object
            ob = ob_inst.object.original
            mat = ob.matrix_world

        print('Analysing ob', ob.name)

        if ob.type=='MESH' and ob.data.polygons:
            if not ob.data.uv_layers : 
                continue
            
            face_index = 0
            uv_data = ob.data.uv_layers.active.data
            world_loc = mat.to_translation()     
            
            if not [v == 0 for v in mat.to_scale()] :
                        
                local_org = mat.inverted()@cam_origin
                
                (hit, loc, norm, face_index) = ob.closest_point_on_mesh(local_org)
                world_loc = mat@ loc
                
            distance = (cam_origin-world_loc).length    
        
            for s in ob.material_slots :
                if not s.material or not s.material.use_nodes : 
                    continue
                
                ratios = []
                for n in s.material.node_tree.nodes :
                    if not n.type == 'TEX_IMAGE' :
                        continue
                    
                    if not n.image or not n.image.filepath:
                        continue

                    ## Find mapping only with mapping node 
                    mapping_scale = n.texture_mapping.scale[0]
                    for l in n.inputs['Vector'].links :
                        if l.from_node.type == 'MAPPING' :
                            mapping_scale *= l.from_node.inputs['Scale'].default_value[0]


                    #print('mapping_scale', mapping_scale)

                    img = n.image
                    
                    path = bpy.path.relpath( bpy.path.abspath(img.filepath, library=img.library) )
                    
                    src_width, src_height = img.size
                    
                    f = ob.data.polygons[face_index]
                    
                    p1, p2 = uv_data[f.loop_indices[0]].uv , uv_data[f.loop_indices[2]].uv 

                    uv_px_length = ((p2-p1)*Vector( (src_width, src_height) ) ).length 
                    uv_px_length *= mapping_scale

                    if uv_px_length < 0.000001 :
                        continue
                    
                    v1 = mat@ob.data.vertices[f.vertices[0]].co 
                    v2 = mat@ob.data.vertices[f.vertices[2]].co
                    angle = atan( (v2 - v1).length / distance )
                    cam_px_length = ( angle / (cam.data.angle) ) * res_width
                    
                    ratio = cam_px_length/uv_px_length
                    ratios.append(ratio)
                    
                    if ratio > 1 :
                        print(f'The texture {path} on object {ob.name} should be {ratio} bigger')
                    
                    if not path in data :
                        data[path] = {
                            'ratio': ratio,
                            'src_width': src_width, 
                            'src_height' : src_height, 
                        }
                    
                    if ratio > data[path]['ratio'] :
                        data[path]['ratio'] = ratio

                ratio = max(ratios) if ratios else 0
                if not 'ratio' in s.material.keys() :
                    s.material['ratio'] = ratio

                if ratio > s.material['ratio'] :
                    s.material['ratio'] = ratio




def set_material_color(override=False):
    color = {
            0.125 : (0,0,1, 1),
            0.25 : (0,0.33,0.66, 1),
            0.5 : (0,0.66,0.33, 1),
            1 : (0,1,0, 1),
            2 : (0.33,0.66,0, 1),
            3 : (0.66,0.33,0, 1),
            4 : (1,0,0, 1),
    }

    for mat in bpy.data.materials :
        if not 'src_color' in mat.keys():
            mat['src_color'] = mat.diffuse_color[:]
            if not override :
                continue

        if 'ratio' in mat.keys() :
            ratio = min(color.keys(), key=lambda x:abs(x-mat['ratio']))
            mat.diffuse_color = color[ratio]


def restore_materials():
     for mat in bpy.data.materials :
        if 'src_color' in mat.keys() :
            mat.diffuse_color = mat['src_color'] 
        if 'ratio' in mat.keys():
            del mat['ratio']


def norm_img_name(name, size, is_udim=False):
    path = Path(name)

    find_list = [f'_{2**i}' for i in range(3,10)] + ['_.K', '_.k']
    img_reg = re.compile('|'.join(find_list))
    udim_reg = re.compile(r'[.|_]1[\d]{3}')

    size_val = str(int(size)) if size < 1024 else f'{str(size)[0]}K'
    
    res = img_reg.findall(path.name)
    if res :
        return path.name.replace(res[0],f'_{size_val}', 1)

    if is_udim:
        udims = udim_reg.findall(str(path))
        if udims:
            norm_name = path.stem.replace(udims[-1], '', -1)
            return f'{norm_name}_{size_val}{udims[-1]}{path.suffix}'
    
    return f'{path.stem}_{size_val}{path.suffix}'


def set_images_size(data, debug_only=False):

    udim_reg = re.compile(r'[.|_](1[\d]{3})')

    print('debug_only', debug_only)

    for img_src_path, img_data in data.items() :
        if img_data['ratio'] >= 1 or img_data['ratio'] == 0 :
            continue
        
        img_path = Path( abspath( bpy.path.abspath(img_src_path) ) )
        if not img_path.exists() :
            continue
        #print('img_path', img_path)

        images = bpy.data.images[:]
        images = [(i, bpy.path.abspath(i.filepath, library=i.library)) for i in images]
        #print('images', images)
        images = [i for i, p in images if Path(abspath(p)) == img_path]

   
        #print('images', images)
        
        for img in images :

            img['src_path'] = img_src_path

            ratio = img_data['ratio']
            
            src_w, src_h = img_data['src_width'], img_data['src_height']

            if src_w <= 0 or src_h <= 0 :
                continue

            #print('src_w', src_w, 'src_h', src_h, 'ratio', ratio)
            dst_w = 2**ceil(log2(src_w * ratio))
            dst_w = max(dst_w, 16) # To have 16px as minimum 
            dst_h = img.size[1]/(src_w/dst_w)

            dst_img_name = norm_img_name(img_path.name, dst_w)
            dst_img_path = img_path.parent/'.auto_texture_size'/dst_img_name
            relative_to = dirname(abspath(bpy.path.abspath(img.library.filepath))) if img.library else None

            if not debug_only :
                dst_img_path.parent.mkdir(exist_ok=True)

            ## For all udim
            udims = udim_reg.findall(str(img_path))
            if img.source == 'TILED' and udims:
                dst_img_name = norm_img_name(img_path.name, dst_w, is_udim=True)
                dst_img_path = img_path.parent/'.auto_texture_size'/dst_img_name

                print('UDIM')
                for tile in img.tiles:
                    udim_src_path = img_path.parent / img_path.name.replace(udims[-1], tile.label, -1)

                    print('udim_src_path', udim_src_path)

                    if udim_src_path.exists() and not debug_only :
                        udim_dst_name = norm_img_name(udim_src_path.name, dst_w, is_udim=True)
                        udim_dst_path = dst_img_path.parent/udim_dst_name

                        print('udim_dst_path', udim_dst_path)

                        if not udim_dst_path.exists() :
                            print('Save udim image to', udim_dst_path)
                            i = bpy.data.images.load(str(udim_src_path)) 
                            try:                           
                                i.scale(dst_w, dst_h)
                                i.filepath_raw = str(udim_dst_path)
                                i.save() 
                            except Exception as e:
                                print(e)
                            
                            bpy.data.images.remove(i)
            
            elif not debug_only and not dst_img_path.exists():
                print('Save image to', dst_img_path)
                try:
                    img.scale(dst_w, dst_h)
                    img.filepath_raw = str(dst_img_path)
                    img.save()
                except Exception as e:
                    print(e)
                        
            relative_to = dirname(abspath(bpy.path.abspath(img.library.filepath))) if img.library else None
            #print('relative_to', relative_to)

            img_data['dst_path'] = bpy.path.relpath( str(dst_img_path),  start=relative_to)
            img.filepath = img_data['dst_path']
                
        