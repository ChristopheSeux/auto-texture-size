
import bpy

from .functions import restore_images, store_optimal_textures, set_images_size,\
set_material_color, restore_materials


class ATS_OT_set_textures_size(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "ats.set_textures_size"
    bl_label = "Set Texture Size"

    def execute(self, context):
        scene = context.scene
        
        data = scene.auto_texture_size.data

        data.clear()

        restore_images()
        restore_materials()

        debug_only = scene.auto_texture_size.debug
        
        store_optimal_textures(data)
        set_images_size(data, debug_only)
        set_material_color(False)

        return {'FINISHED'}


class ATS_OT_restore_textures(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "ats.restore_textures"
    bl_label = "Restore Texture Size"

    def execute(self, context):
        scene = context.scene
        data = scene.auto_texture_size.data

        restore_images()

        return {'FINISHED'}


class ATS_OT_set_materials_color(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "ats.set_materials_color"
    bl_label = "Set Material Color"

    def execute(self, context):
        scene = context.scene
        data = scene.auto_texture_size.data

        set_material_color(True)

        return {'FINISHED'}

class ATS_OT_restore_materials(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "ats.restore_materials"
    bl_label = "Restore Material Color"

    def execute(self, context):
        scene = context.scene
        data = scene.auto_texture_size.data

        restore_materials()

        return {'FINISHED'}