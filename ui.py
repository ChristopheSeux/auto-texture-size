
import bpy
from bpy.types import Panel


class ATS_PT_panel(Panel):
    bl_label = 'Auto Texture Size'
    bl_space_type = 'PROPERTIES'
    bl_region_type= 'WINDOW'
    bl_context = 'render'

    def draw(self, context):
        col = self.layout.column()
        props = context.scene.auto_texture_size

        col.prop(props, 'debug')
        col.operator("ats.restore_textures")
        col.operator("ats.set_textures_size")
        col.separator()
        col.operator("ats.restore_materials")
        col.operator("ats.set_materials_color")
